package main

import (
	"fmt"
	"hash"
	"io"
	"strings"
)

type Hasher interface {
	Name() string
	CalculateHash(io.Reader) (string, error)
	GetChecksum() (string, error)
	VerifyChecksum(checksum string) bool
}

func NewAlgorithm(name string, algo hash.Hash) Hasher {
	return &Algorithm{name: strings.ToUpper(name), algo: algo, checksum: nil}
}

type Algorithm struct {
	name     string
	algo     hash.Hash
	checksum *string
}

func (algo *Algorithm) Name() string {
	return algo.name
}

func (algo *Algorithm) CalculateHash(r io.Reader) (string, error) {
	if _, err := io.Copy(algo.algo, r); err != nil {
		return "", err
	}

	tmp := strings.ToLower(fmt.Sprintf("%x", algo.algo.Sum(nil)))
	algo.checksum = &tmp

	return *algo.checksum, nil
}

func (algo *Algorithm) GetChecksum() (string, error) {
	if algo.checksum == nil {
		return "", fmt.Errorf("no checksum calculated")
	}
	return *algo.checksum, nil
}

func (algo *Algorithm) VerifyChecksum(checksum string) bool {
	return strings.ToLower(*algo.checksum) == strings.ToLower(checksum)
}
