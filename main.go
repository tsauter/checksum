package main

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"
)

var (
	AvailableHashers []Hasher
	DefaultHashers   []Hasher
	flagVerifyHash   *string
)

func init() {
	AvailableHashers = []Hasher{
		NewAlgorithm("md5", md5.New()),
		NewAlgorithm("sha1", sha1.New()),
		NewAlgorithm("sha256", sha256.New()),
		NewAlgorithm("sha512", sha512.New()),
	}

	DefaultHashers = []Hasher{
		NewAlgorithm("sha1", sha1.New()),
		NewAlgorithm("sha256", sha256.New()),
	}
}

func listHashes() {
	var hashes []string
	for _, hasher := range AvailableHashers {
		hashes = append(hashes, hasher.Name())
	}
	sort.Strings(hashes)

	var defaultHashes []string
	for _, hasher := range DefaultHashers {
		defaultHashes = append(defaultHashes, hasher.Name())
	}

	sort.Strings(hashes)
	fmt.Printf("Supported hash algorithms:\n")
	fmt.Printf("(* default algorithm)\n")
	for _, name := range hashes {
		mark := ""
		if stringInSlice(strings.ToUpper(name), defaultHashes) {
			mark = "*"
		}
		fmt.Printf("  - %s%s\n", name, mark)
	}

	fmt.Printf("\n\n")
}

func validateFile(filename string, enabledHashers []Hasher) {
	f, err := os.Open(filename)
	if err != nil {
		fmt.Printf("Failed to open file: %v\n", err)
		os.Exit(1)
	}
	defer f.Close()

	for _, hasher := range enabledHashers {
		f.Seek(0, 0)
		hash, err := hasher.CalculateHash(f)
		if err != nil {
			fmt.Printf("Failed to read file: %v\n", err)
			os.Exit(1)
		}
		fmt.Printf("%s: %s: %s\n", filename, hasher.Name(), hash)
	}

	if *flagVerifyHash != "" {
		verifyOK := false

		for _, hasher := range enabledHashers {
			if hasher.VerifyChecksum(*flagVerifyHash) {
				fmt.Printf("%s: %s: %s\n", filename, "VERIFY", hasher.Name())
				verifyOK = true
			}
		}

		if !verifyOK {
			fmt.Printf("%s: %s: %s\n", filename, "VERIFY", "failed for all hash sizes")
		}
	}

}

func main() {
	var enabledHashers = DefaultHashers

	showHashesOnly := flag.Bool("show-algorithms", false, "Only show supported algorithms and exit.")
	useAllHashes := flag.Bool("all", false, "Generate hash for all available hash sizes.")
	useSpecificHash := flag.String("hash", "", "Generate hash for the specified hash size.")
	flagVerifyHash = flag.String("verify", "", "Validate calculated hashes against this hash.")
	flag.Parse()

	if *showHashesOnly {
		listHashes()
		os.Exit(1)
	}

	if len(flag.Args()) < 1 {
		fmt.Printf("No file specified.\n")
		os.Exit(1)
	}

	if *useAllHashes && (*useSpecificHash != "") {
		fmt.Printf("Parameter -all and -hash can not be used together.\n")
		os.Exit(1)
	}

	if *useAllHashes {
		enabledHashers = AvailableHashers
	}

	if *useSpecificHash != "" {
		enabledHashers = []Hasher{}

		for _, h := range AvailableHashers {
			if h.Name() == strings.ToUpper(*useSpecificHash) {
				enabledHashers = append(enabledHashers, h)
			}
		}

		if len(enabledHashers) < 1 {
			fmt.Printf("Invalid or unsupported hash specified: %s.\n", *useSpecificHash)
			os.Exit(1)
		}
	}

	for _, filename := range flag.Args() {
		validateFile(filename, enabledHashers)
	}
}
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
